package org.nudge.probe;

import java.lang.annotation.Annotation;
import java.util.List;

public interface MethodCall {

	/**
	 * @return method full name
	 */
	String getMethodFullName();

	/**
	 * @return method name
	 */
	String getMethodName();

	/**
	 * @return method class name
	 */
	String getClassName();

	/**
	 * @return class package name
	 */
	String getPackageName();

	/**
	 * get method parameter by its index
	 *
	 * @param i    parameter index
	 * @param type parameter type
	 * @param <T>  parameter class
	 * @return method parameter value
	 */
	<T> T getMethodParam(int i, Class<T> type);

	<T extends Annotation> T getParamAnnotation(int i, Class<T> annotationType);
	<T extends Annotation> T getMethodAnnotation(Class<T> annotationType);

	/**
	 * @param type object type
	 * @param <T>  object class
	 * @return method target object, null for static methods
	 */
	<T> T getMethodTarget(Class<T> type);

	/**
	 * @return method parameters
	 */
	List<Object> getMethodParams();

	/**
	 * @return method return value, if any, {@link java.lang.Void} otherwise
	 */
	<T> T getReturnValue(Class<T> type);

	/**
	 * @return method thrown exception, if any, null otherwise
	 */
	Throwable getThrownException();

	/**
	 * @return true when method threw exception, false otherwise
	 */
	boolean hasThrownException();

}
