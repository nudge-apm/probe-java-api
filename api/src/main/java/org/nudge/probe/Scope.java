package org.nudge.probe;

public enum Scope {
	TRANSACTION,
	LAYER
}