package org.nudge.probe;

import java.util.Collections;
import java.util.Map;

public class CustomHandler {

	protected final MethodCall methodCall;

	public CustomHandler(MethodCall methodCall) {
		this.methodCall = methodCall;
	}

	public String getType() {
		return methodCall.getMethodAnnotation(Trace.class).type();
	}

	public String getCode() {
		String annotationName = methodCall.getMethodAnnotation(Trace.class).value();
		if (null != annotationName && annotationName.length() > 0) {
			return annotationName;
		}
		return methodCall.getMethodFullName();
	}

	public Map<String, String> getExtendedCodes() {
		return Collections.emptyMap();
	}

}