package org.nudge.probe;

import java.lang.annotation.*;

@Documented
@Target(value = {ElementType.METHOD, ElementType.CONSTRUCTOR})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Trace {

	String value() default "";

	Scope scope() default Scope.TRANSACTION;

	String type() default "@Trace";

	Class<? extends CustomHandler> handler() default CustomHandler.class;

}