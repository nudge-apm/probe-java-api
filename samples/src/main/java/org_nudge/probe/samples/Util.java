package org_nudge.probe.samples;

public class Util {
	static void doStuff(long wait, String msg) {
		try {
			Thread.sleep(wait);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(String.format(" >> %s", msg));
	}
}
