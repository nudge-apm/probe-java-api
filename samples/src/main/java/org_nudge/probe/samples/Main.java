// by default org.nduge.* package is not instrumented, thus we use another
package org_nudge.probe.samples;

public class Main {

	public static void main(String[] args) {
		String action = "sample1";
		if (args.length == 1) {
			action = args[0];
		}

		Samples samples = new Samples();

		if ("sample1".equals(action)) {
			samples.sample1();
		} else if ("sample2".equals(action)) {
			samples.sample2();
		} else if ("sample3".equals(action)) {
			samples.sample3();
		} else {
			System.err.println(String.format("unknown action : %s", action));
		}


	}

}
