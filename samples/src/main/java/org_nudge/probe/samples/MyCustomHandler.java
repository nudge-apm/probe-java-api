package org_nudge.probe.samples;

import org.nudge.probe.CustomHandler;
import org.nudge.probe.MethodCall;

public class MyCustomHandler extends CustomHandler {

	public MyCustomHandler(MethodCall methodCall) {
		super(methodCall);
	}

	// Implementation guidelines :
	//
	// "With Great Power Comes Great Responsibility"
	//
	// - this code will be executed on every instrumented method call,
	// thus it is recommended to minimize overhead.
	//
	// - in order to be effective, the number of distinct transactions and
	// layer interactions should remain relatively low. For example, tracing all parameters
	// can create a very large number of distinct items and is thus not recommended.

	@Override
	public String getType() {
		// defaults to '@Trace'
		// you can overload in order to dynamically choose which transaction type / layer
		// depending on method call.

		return "sample3";
	}

	@Override
	public String getCode() {
		// defaults to method signature
		//
		// here we only overload layer interactions, not transactions
		String methodName = methodCall.getMethodName();

		if (!methodName.equals("sample3_layer")) {
			return super.getCode();
		} else {
			// retrieve method arguments & return value
			String argument = methodCall.getMethodParam(0, String.class);
			String returnValue = methodCall.getReturnValue(String.class);
			return String.format("%s( %s ) -> %s", methodName, argument, returnValue);
		}

	}
}
