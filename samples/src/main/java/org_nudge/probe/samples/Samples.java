package org_nudge.probe.samples;

import org.nudge.probe.Scope;
import org.nudge.probe.Trace;

import static org_nudge.probe.samples.Util.doStuff;

public class Samples {

	// method annotated with @Trace
	//
	// will create a transaction in nudge (scope is transaction by default)
	@Trace
	public void sample1() {
		doStuff(20, "sample1");
	}

	// transaction with a custom layer
	//
	// will create a transaction in nudge with an '@Trace' layer
	// each time this transction is executed there are two
	// interactions on '@Trace'  layer and one on 'MyLayer'.
	@Trace
	public void sample2() {
		sample2_layer1();
		sample2_layer2();
		sample2_layer3();
	}

	// this layer interaction will use default naming policy
	@Trace(scope = Scope.LAYER)
	private void sample2_layer1() {
		doStuff(10, "sample2");
	}

	// this layer interaction will use a custom naming policy
	@Trace(scope = Scope.LAYER, value = "Sample2 Layer2")
	private void sample2_layer2() {
		doStuff(10, "sample2");
	}

	// this layer interaction will use non-default layer name
	@Trace(scope = Scope.LAYER, type = "MyLayer")
	private void sample2_layer3() {
		doStuff(10, "sample2");
	}

	// advanced customization with code at transaction level
	// and at layer level
	public void sample3() {
		sample3_tx("first");
		sample3_tx("second");
	}

	@Trace(handler = MyCustomHandler.class)
	public void sample3_tx(String value) {
		doStuff(30, value);
		sample3_layer("hello");
		sample3_layer("world");
	}

	@Trace(handler = MyCustomHandler.class, scope = Scope.LAYER)
	public String sample3_layer(String value) {
		doStuff(10, value);
		return value.length() % 2 == 0 ? "even" : "odd";
	}

}
