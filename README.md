# Nudge APM - Java probe API #

### Who should use this ? ###

* You use www.nudge-apm.com monitoring
* Your application uses protocols that don't fit JEE standards like EJB, JMS, JAX-WS, ...
* You want to customize how transactions are monitored : more details, dynamic naming, display parameters or return values, ...

If you answered 'Yes' to at least 2 of above statements, you are in the right place.

### One minute tutorial ###

Add probe API as dependency

```
  <dependency>
    <groupId>org.nudge.apm</groupId>
    <artifactId>probe-java-api</artifactId>
    <version>3.0</version>
  </dependency>
```

Annotate methods with `@Trace`

Deploy your application as usual, you should see annotated methods appear as transactions (if they weren't already).

Once properly set up, you can start to customize how transactions are monitored using provided samples : [Samples.java](https://bitbucket.org/nudge-apm/probe-java-api/src/master/samples/src/main/java/org_nudge/probe/samples/Samples.java?at=master&fileviewer=file-view-default).

### Need Help ? ###

contact helpdesk@nudge.org.